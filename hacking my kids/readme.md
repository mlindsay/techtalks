# hacking my children

This is a talk about hacking children. In the good way. Using rhetorical tools, boundaries, listening and love to help them grow into somewhat independent grown-up humans. Ideally.

## background

- story from Vanuatu (kids understanding faces, and your reactions to things, from a young age)

- invest in good behaviour and establish good patterns and you can be more lazy later

## sensible things

- kids are lawyers (not a compliment)
  - they remember everything
  - they draw on any precedent they can think of, however tangential
  - they will divide and conquer and leverage others (e.g. grandparents) to get what they want

- they have more time and energy than you so you need to ensure you manage that wisely

### fun games to address the energy imbalance

- Bring me solo and the wookiee
- Unsnuggle
- I'm So Tired
- Chair Game
- Roll You Over

### trust

- they need to trust you

- promises vs threats? same thing?

NO going back on your word, but they can convince you. I try to incentivise them to persuade me.

Things that are *not* persuasive

- tantrums / screaming
- whining ("I can't hear you when you talk like that")
- threats

### incentivise the right things

- screamy tantrums don't work
- nice words do

$10 pot to clean the car. If one kid does all the work, they get $10. Otherwise split up by ratio of work done (you can see who's doing the work and who's pretending)

### de-escalate when possible

- park incident

### set very clear tasks

- "tidy the lounge" is ambiguous and a bit wooly and open to interpretation and kids are lawyers

- pick up 25 things
  - all the pencil crayons count as 1
  - all the papers count as one

## hacks

### same team parenting

if you have a partner: "I'm OK with it if your dad's OK with it" means there is dialogue rather than divide-and-conquer strategy.

I totally used this on my own Dad.

- story: door-slamming kid and parents not working together.

### handling whining

Move to future tense.

- _whining_ "yes, you've stated your problem" _more whining_ "I already understand your problem. what would you like from me?"

### false dichotomy

- red shirt or blue shirt?
  - no shirt isn't an option
  - that stained shirt from yesterday also isn't an option
  - whatever kid, just pick one already, idk

### forced teaming

- arguing in the car
  - me: "Do I hear arguing kids?"
  - them (both): NO
  - me: good, I can't take arguing kids out for ice cream

### defer to The Timer

- 1: may I have ice cream?
- me: you can have an ice cream in 30 minutes

if they push back: ice cream in 30 minutes or no ice cream at all today. Soon they learn that pushing back (and/or losing temper) doesn't work and makes things worse.

When leaving the park: Set a timer. 10 minutes. 5 minutes. 1 minute, 5, 4, 3, 2, 1.

Works great for getting your kids to leave you alone so you can have focus time.

### defer to The List

- 1: may I play minecraft?
- me: what does the List say? Is everything complete?

The List has predefined tasks that are prerequisites for any screen time

### defer to The Puppet

- Tassie asks things and they engage
- tell Tassie what happened
- explain this to Tassie (socratic method)

### apologising

- multiple stories about things being accidentally thrown at my head and how the parents responded
- making kids apologise defines blameless protocol
  - not about you it's about checking in with the other person

#### apology protocol - 4 parts

"sorry" is half of the first step and it's better than nothing

- acknowledge: I threw the ball and it hit you by accident
- check in: are you ok?
- help: is there anything I can do to help you feel better?
- future: say how you'll avoid in future (e.g. next time I'll be more careful)

## my kids hack me right back

- 1: "mum you look tired. are you ok?"
- me: "yes, I am tired. Thank you for checking in."
- 1: "OK, you stay and rest. I'll go get the ice cream"

## books

- Heinrichs _Thank You For Arguing_
- Faber, Mazlish _How to Talk so Kids Will Listen and Listen so Kids Will Talk_
